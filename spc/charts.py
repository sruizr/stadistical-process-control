import statistics
from collections import deque


class ControlChart:
    def __init__(self, mean=None, stdev=None):
        self._data = deque()
        self._mean = mean
        self._stdev = stdev
        self._rules = [
            'rule_beyond_limits',
            'rule_zone_a',
            'rule_zone_b',
            'rule_zone_c',
            'rule_trend',
            'rule_mixture',
            'rule_stratification',
            'rule_over_control']

    def add_sample(self, value):
        self._data.appendleft(value)
        sampling_size = len(self._data)

        if sampling_size > 25:
            self._data.pop()

    @property
    def sampling_size(self):
        return len(self._data)

    @property
    def mean(self):
        if self._mean is not None:
            return self._mean
        else:
            return statistics.mean(self._data)

    def is_unstable(self, number_of_rules=8):
        if self.sampling_size < 5:
            return True

        for index in range(number_of_rules):
            rule = getattr(self, self._rules[index])
            result = rule()
            if result is not None:
                return (index + 1, result)

        return False

    def _beyond_control_limits(self, index):
        return abs(self._data[index] - self.mean) > 3 * self._stdev

    def _in_zone_a_or_beyond(self, index):
        return abs(self._data[index] - self.mean) > 2 * self._stdev

    def _in_zone_b_or_beyond(self, index):
        return abs(self._data[index] - self.mean) > self._stdev

    def _in_zone_c(self, index):
        return  abs(self._data[index] - self.mean) < self._stdev

    def _over_mean(self, index):
        return self._data[index] > self.mean

    def _trend(self, index):
        increase = self._data[index] - self._data[index + 1]
        if increase:
            return int(increase/abs(increase))
        return 0

    def rule_beyond_limits(self):
        """One or more points beyond the control limits"""

        for index in range(5):
            if self._beyond_control_limits(index):
                return index

    def rule_zone_a(self):
        """2 out of 3 consecutive points in Zone A or beyond
        """
        counter = 0
        for index in range(3):
            if self._in_zone_a_or_beyond(index):
                counter += 1
            if counter == 2:
                return index

    def rule_zone_b(self):
        """4 out of 5 consecutive points in Zone B or beyond
        """
        counter = 0
        for index in range(5):
            if self._in_zone_b_or_beyond(index):
                counter += 1
            if counter == 4:
                return index

    def rule_zone_c(self):
        """
        7 or more consecutive points on one side of the average (in Zone C or beyond)
        """
        if self.sampling_size < 7:
            return False

        side = self._over_mean(0)
        for index in range(7):
            if self._over_mean(index) != side:
                return
        return index

    def rule_trend(self):
        """7 consecutive points trending up or trending down
        """
        if self.sampling_size < 8:
            return False

        trend = self._trend(0)
        for index in range(7):
            if trend != self._trend(index):
                return
        return index

    def rule_mixture(self):
        """8 consecutive points with no points in Zone C
        """
        if self.sampling_size < 8:
            return False

        for index in range(8):
            if self._in_zone_c(index):
                return

        return index

    def rule_stratification(self):
        """15 consecutive points in Zone C
        """
        if self.sampling_size < 15:
            return False

        for index in range(15):
            if not self._in_zone_c(index):
                return

        return index

    def rule_over_control(self):
        """14 consecutive points alternating up and down
        """
        side = 0
        for index in range(15):
            if self._over_mean(index) == side:
                return

        return index
