import numpy as np
from spc.charts import ControlChart


def fill_sample_data():
    control_chart = ControlChart(stdev=1.0, mean=0.0)
    samples = np.random.normal(size=15)

    for sample in samples:
        control_chart.add_sample(sample)

    if control_chart.is_unstable():
        control_chart = fill_sample_data()

    return control_chart

def test_rule_beyond_limits():
    control_chart = fill_sample_data()
    control_chart.add_sample(4)
    assert control_chart.is_unstable() == (1, 0)


def test_rule_zone_a():
    control_chart = fill_sample_data()
    control_chart.add_sample(2.1)
    control_chart.add_sample(2.1)

    assert control_chart.is_unstable() == (2, 1)


def test_rule_zone_b():
    control_chart = fill_sample_data()
    control_chart.add_sample(1.5)
    control_chart.add_sample(1.5)
    control_chart.add_sample(1.5)
    control_chart.add_sample(1.5)

    assert control_chart.is_unstable() == (3, 3)


def test_rule_zone_c():
    control_chart = fill_sample_data()
    for _ in range(7):
        control_chart.add_sample(0.8)

    assert control_chart.is_unstable() == (4, 6)


def test_rule_trend():
    control_chart = fill_sample_data()
    incr = 0.01
    for n in range(8):
        control_chart.add_sample(n * incr - 0.05)

    assert control_chart.is_unstable() == (5, 6)


def test_rule_mixture():
    control_chart = fill_sample_data()



def test_rule_stratification():
    pass


def test_rul_over_control():
    pass
